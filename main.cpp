#include "sortingAlgorithms.hpp"
#include <algorithm>    // shuffle
#include <vector>
#include <random>       // default_random_engine
#include <chrono> 
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std::chrono;

int** instanceGenerator(int n);
void vetMaker ();

int main(int argc, char *argv[ ])
{
	if(argc != 5) {
		cout << "wrong input, try './exe n(instance size) sortAlgorithm(1:merge sort   2:insetion sort   3: radix sort) file(0: genarate files   1: don't generate files) time (1: seconds  2: milliseconds)'" << endl;
		return 1;
	}

	if(argc == 4 && argv[3] == 0)
		vetMaker();

	int n = atoi(argv[1]);

	duration<double> time, total;

	int **v = instanceGenerator(n);

	for (int i = 0; i < 20; i++)
	{

		if(atoi(argv[2]) == 1) {
			high_resolution_clock::time_point t1 = high_resolution_clock::now();
			mergeSort(v[i], 0, n);
			high_resolution_clock::time_point t2 = high_resolution_clock::now();
			time = duration_cast<duration<double> >(t2 - t1);
			total += duration_cast<duration<double> >(t2 - t1);
		}
		else if(atoi(argv[2]) == 2) {
			high_resolution_clock::time_point t1 = high_resolution_clock::now();
			insertionSort(v[i], n);
			high_resolution_clock::time_point t2 = high_resolution_clock::now();
			time = duration_cast<duration<double> >(t2 - t1);
			total += duration_cast<duration<double> >(t2 - t1);
		}
		else {
			high_resolution_clock::time_point t1 = high_resolution_clock::now();
			radixSort(v[i], n);
			high_resolution_clock::time_point t2 = high_resolution_clock::now();
			time = duration_cast<duration<double> >(t2 - t1);
			total += duration_cast<duration<double> >(t2 - t1);
		}
		
		if(atoi(argv[4]) == 2)
			printf("\t\ti: %d -- tempo: %lf ms\n", i, time.count() * 1000);
		else
			printf("\t\ti: %d -- tempo: %lf s \n", i, time.count());
	}
	if(atoi(argv[4]) == 2)
		printf("Tempo total de execucao : %lf ms \n", total.count() * 1000);
	else
		printf("Tempo total de execucao : %lf s \n", total.count());
	
	for(int i = 0; i < 20; i++)
		delete v[i];
	delete v;

	return 0;
}

int** instanceGenerator(int n) {

	int** instance = new int*[20];

	string name = "a.txt";
    srand(time(NULL));

	for(int i = 0; i < 20; i++) {
		instance[i] = new int[n];
		name[0]++;
        ifstream myfile(name.c_str());
		
		for(int j = 0; j < n; j++) {
			myfile >> instance[i][j];
		}
		myfile.close();
	}
		

	return instance;	
}

void vetMaker () {
	string name = "a.txt";
    srand(time(NULL));
    for(int i = 1; i <= 20; i++) {
        name[0]++;
        ofstream myfile(name.c_str());
        cout << "file: " << name << " start" << endl;
        for(int j = 0; j < 1000000; j++)
            myfile << rand() % 1000000 << "\n";
        cout << "file: " << name << " finish" << endl;
        myfile.close();
    }
}