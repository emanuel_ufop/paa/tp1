#!/bin/sh

echo "compilando..."
g++ -std=c++11 main.cpp sortingAlgorithms.cpp -Wall -o exe
echo "criando pasta test"
mkdir test
echo "---------------  MERGE SORT --------------"
echo "executando n = 10"
./exe 10 1 0 2 > ./test/mergeN10.txt

echo "executando n = 100"
./exe 100 1 1 2 > ./test/mergeN100.txt

echo "executando n = 1000"
./exe 1000 1 1 2 > ./test/mergeN1000.txt

echo "executando n = 10000"
./exe 10000 1 1 2 > ./test/mergeN1000.txt

echo "executando n = 100000"
./exe 100000 1 1 2 > ./test/mergeN10000.txt

echo "executando n = 1000000"
./exe 1000000 1 1 2 > ./test/mergeN100000.txt



echo "--------------- INSERTION SORT ----------------"

echo "executando n = 10"
./exe 10 2 1 2 > ./test/insertionN10.txt

echo "executando n = 100"
./exe 100 2 1 2 > ./test/insertionN100.txt

echo "executando n = 1000"
./exe 1000 2 1 2 > ./test/insertionN1000.txt

echo "executando n = 10000"
./exe 10000 2 1 2 > ./test/insertionN10000.txt

echo "executando n = 100000"
./exe 100000 2 1 2 > ./test/insertionN100000.txt

echo "executando n = 1000000"
./exe 1000000 2 1 2 > ./test/insertionN1000000.txt



echo "------------------ RADIX SORT -----------------"

echo "executando n = 10"
./exe 10 3 1 2 > ./test/radixN10.txt

echo "executando n = 100"
./exe 100 3 1 2 > ./test/radixN100.txt

echo "executando n = 1000"
./exe 1000 3 1 2 > ./test/radixN1000.txt

echo "executando n = 10000"
./exe 10000 3 1 2 > ./test/radixN10000.txt

echo "executando n = 100000"
./exe 100000 3 1 2 > ./test/radixN100000.txt

echo "executando n = 1000000"
./exe 1000000 3 1 2 > ./test/radixN1000000.txt

sleep 1000000000000