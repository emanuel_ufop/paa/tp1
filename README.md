# PAA - TP1

## Compilando
    - g++ -std=c++11 main.cpp sortingAlgorithms.cpp -Wall -o exe
## Executando
    ./exe <n> <s> <f> <t>
    - n: número de elemento
    - s: algoritmo
        - 1: merge sort
        - 2: insertion sort
        - 3: radix sort
    - f: gerar novo arquivo para salvar vetores (necessário na primeira execução)
        - 0: gera arquivos
        - 1: não gera arquivos
    - t: formato do tempo
        - 1: segundos
        - 2: milesegundos 